from flask import Flask, render_template, Response
import airdrum
import cv2

app = Flask(__name__)
cap = cv2.VideoCapture(0)


@app.route('/')
def index():
    return render_template("index.html")


def gen(capture):
    while True:
        frame = airdrum.main(capture)
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')


@app.route('/video_feed')
def video_feed():
    return Response(gen(cap),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port='8000', debug=True)
