import cv2
import numpy as np
import simpleaudio as sa

hihat_color = (255, 0, 0)
bass_color = (0, 255, 0)
snare_color = (0, 0, 255)

drum_radius = 100

blue_lower = np.array([95, 60, 94], np.uint8)
blue_upper = np.array([163, 168, 209], np.uint8)

play_hihat = False
play_bass = False
play_snare = False

prev_state = "none"

prev_ellipse = (0, 0)


def playAudio():
    global play_hihat, play_bass, play_snare, prev_state

    if play_hihat and prev_state != "hihat":
        file_name = "ses/hi_hat.wav"
        wave_obj = sa.WaveObject.from_wave_file(file_name)
        wave_obj.play()
        prev_state = "hihat"
    if play_bass and prev_state != "bass":
        file_name = "ses/Open-Hi-Hat.wav"
        wave_obj = sa.WaveObject.from_wave_file(file_name)
        wave_obj.play()
        prev_state = "bass"
    if play_snare and prev_state != "snare":
        file_name = "ses/snare.wav"
        wave_obj = sa.WaveObject.from_wave_file(file_name)
        wave_obj.play()
        prev_state = "snare"


def in_circle(center_x, center_y, radius, x, y):
    square_dist = (center_x - x) ** 2 + (center_y - y) ** 2
    return square_dist <= radius ** 2


def checkCollision(center, drum):
    global play_hihat, play_bass, play_snare, prev_state
    drum_name = drum[0]
    drum_pos = drum[1]
    x = in_circle(center[0], center[1], drum_radius, drum_pos[0], drum_pos[1])
    if x:
        if drum_name == "hihat":
            if not play_hihat:
                play_hihat = True
                play_bass = False
                play_snare = False
        if drum_name == "bass":
            if not play_bass:
                play_bass = True
                play_hihat = False
                play_snare = False
        if drum_name == "snare":
            if not play_snare:
                play_snare = True
                play_hihat = False
                play_bass = False
    else:
        play_hihat = False
        play_bass = False
        play_snare = False
        if drum_name == "hihat" and prev_state == "hihat":
            prev_state = "none"
        if drum_name == "bass" and prev_state == "bass":
            prev_state = "none"
        if drum_name == "snare" and prev_state == "snare":
            prev_state = "none"

    playAudio()


def drawEllipse(img, contours, text):
    global prev_ellipse
    if contours is None or len(contours) == 0:
        return prev_ellipse, None
    c = max(contours, key=cv2.contourArea)
    ((x, y), radius) = cv2.minEnclosingCircle(c)
    if cv2.contourArea(c) < 500:
        return prev_ellipse, None
    ellipse = cv2.fitEllipse(c)
    cv2.ellipse(img, ellipse, (0, 0, 0), 2)

    blank = np.zeros(img.shape[0:2])
    ellipseImage = cv2.ellipse(blank, ellipse, (255, 255, 255), -2)

    M = cv2.moments(c)
    if M["m00"] == 0:
        return
    center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))

    if radius > 10:
        cv2.circle(img, center, 3, (0, 0, 255), -1)
        cv2.putText(img, text, (center[0] + 10, center[1]),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 0, 0), 2)
        cv2.putText(img, "(" + str(center[0]) + "," + str(center[1]) + ")", (center[0] +
                                                                             10, center[1] + 15),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 0, 0), 1)
    prev_ellipse = center
    return center, ellipseImage


def createDrum(frame, name, pos, radius, color):

    drum = cv2.circle(frame, pos, radius, color, thickness=5)

    return name, pos


def main(cap):

    width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
    height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)

    hihatPos = (int(width)-drum_radius, int(height)-drum_radius)
    bassPos = (int(width/2), int(height)-drum_radius)
    snarePos = (drum_radius, int(height)-drum_radius)

    ret, frame = cap.read()
    frame = cv2.flip(frame, 1)

    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    blue = cv2.inRange(hsv, blue_lower, blue_upper)
    (_, contours, hierarchy) = cv2.findContours(
        blue, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    (blueCenter, blueEllipse) = drawEllipse(frame, contours, "Blue")

    hihat = createDrum(frame, "hihat", hihatPos, drum_radius, hihat_color)
    bass = createDrum(frame, "bass", bassPos, drum_radius, bass_color)
    snare = createDrum(frame, "snare", snarePos, drum_radius, snare_color)

    for drum in [hihat, bass, snare]:
        checkCollision(blueCenter, drum)

    # must be converted into bytes to show it on web
    _, jpeg = cv2.imencode('.jpg', frame)
    return jpeg.tobytes()
