function onLoad() {
  document.getElementById("video_capture").style.visibility = "hidden";
  document.getElementById("video_capture").style.width = "0%";
  document.getElementById("video_capture").style.height = "0%";

  document.getElementById("exit_button").style.visibility = "hidden";
  document.getElementById("exit_button").style.width = "0%";
  document.getElementById("exit_button").style.height = "0%";
}

function onPlay() {
  document.getElementById("play_button").style.visibility = "hidden";
  document.getElementById("play_button").style.width = "0%";
  document.getElementById("play_button").style.height = "0%";
  document.getElementById("play_button").style.position = "absolute";

  document.getElementById("video_capture").style.visibility = "visible";
  document.getElementById("video_capture").style.width = "100%";
  document.getElementById("video_capture").style.height = "100%";

  document.getElementById("exit_button").style.visibility = "visible";
  document.getElementById("exit_button").style.width = "200px";
  document.getElementById("exit_button").style.height = "50px";
}
function onExit() {
  onLoad();
  document.getElementById("play_button").style.visibility = "visible";
  document.getElementById("play_button").style.width = "200px";
  document.getElementById("play_button").style.height = "50px";
  document.getElementById("play_button").style.position = "unset";
}
